import area
from perimeter import circle

# import area as ar # as keyword is alias
# import numpy as np
# import pandas as pd



print("Area of a circle is : {}".format(area.circle(10))) #string format

print(f"Area of the circle is: {area.circle(10)}") #f string


print(f"Perimeter of a circle is : {round(circle(10), 1)}")